import * as d3 from 'd3';

//Constantes
let angle, offset, data, root, color,
    svgContainer,
    size = 8,
    radius = 7.5,
    dispersion = 2;

export function drawHeaderDesktopChart() {
    //Estado inicial
    setInitState();
}

//Estado inicial
function setInitState() {
    let width = document.getElementById('header-chart').clientWidth,
        height = document.getElementById('header-chart').clientHeight;
    
    svgContainer = d3.select('#header-chart').append("svg")
        .attr("width", width)
        .attr("height", height);
    
    data = d3.range(200).map(function() {
        angle = Math.random() * Math.PI * 2;
        offset = size + radius + dispersion;
        return {
          x: offset + Math.cos(angle) * radius + rand(-dispersion, dispersion),
          y: offset + Math.sin(angle) * radius + rand(-dispersion, dispersion),
          radius: radius
        };      
    });

    root = data[0],
    color = ['red', 'blue', 'green', 'grey', 'pink', 'purple'];

    root.radius = 0;
    root.fixed = true;

    root.px = width / 2; //Center x
    root.py = height / 2; //Center y

    svgContainer
        .selectAll("circle")
        .data(data)
        .enter()
        .append("circle")
        .attr('cx', function (d) {
            return d.x;
        })
        .attr('cy', function (d) {
            return d.y;
        })
        .attr('r', function (d) {
            return d.radius;
        })
        .attr('fill', function (d, i) {
            return color[i % 3];
        });

    let simulation = d3.forceSimulation()
        .alphaTarget(0.05) // stay hot
        .velocityDecay(0.05) // low friction
        .force("x", d3.forceX().strength(0.015))
        .force("y", d3.forceY().strength(0.015))
        .force("collide", d3.forceCollide().radius(d => d.radius).iterations(100))
        .force("charge", d3.forceManyBody().strength((d, i) => i ? 0 : -width * 2.75 / 3))
        .force("center", d3.forceCenter()
            .x(document.getElementById('header-chart').clientWidth / 2)
            .y(document.getElementById('header-chart').clientHeight / 2)
        );
    
    simulation
        .nodes(data)
        .on("tick", function(){
            let q = d3.quadtree(data), i = 0, n = data.length;
        
            while (++i < n) q.visit(collide(data[i]));
            
            svgContainer
                .selectAll("circle")
                .attr("cx", function(d) {
                return d.x;
                })
                .attr("cy", function(d) {
                return d.y;
                });            
        });
    
    //Milisegundos de prueba
    setTimeout(() => {
        simulation.stop();
    }, 20000);
}

//Helpers
function rand(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}
  
function collide(node) {
    let r = node.radius + 16,
        nx1 = node.x - r,
        nx2 = node.x + r,
        ny1 = node.y - r,
        ny2 = node.y + r;

    return function(quad, x1, y1, x2, y2) {
        if (quad.point && (quad.point !== node)) {
        var x = node.x - quad.point.x,
            y = node.y - quad.point.y,
            l = Math.sqrt(x * x + y * y),
            r = node.radius + quad.point.radius;
        if (l < r) {
            l = (l - r) / l * .5;
            node.x -= x *= l;
            node.y -= y *= l;
            quad.point.x += x;
            quad.point.y += y;
        }
        }
        return x1 > nx2 || x2 < nx1 || y1 > ny2 || y2 < ny1;
    };
}

function shuffle(array) {
    let currentIndex = array.length, randomIndex;

    while (0 !== currentIndex) {
        randomIndex = Math.floor(Math.random() * currentIndex);
        currentIndex--;
        [array[currentIndex], array[randomIndex]] = [array[randomIndex], array[currentIndex]];
    }

    return array;
}